# Example GITLAB Docker REGISTRY

## Getting started

This repository is an example of Docker repository.

## Prerequisities

Existing image on the computer.

Example image with docker-compose.yml:
```
version: '3.8'
services:
  php-apache-environment:
    container_name: NETTE_apache
    image: thecodingmachine/php:8.2-v4-apache
    volumes:
      - ./:/var/www/html
    ports:
      - 80:80
    links:
      - database
    depends_on:
      - database
    environment:
      APACHE_DOCUMENT_ROOT: /var/www/html
      NETTE_DEBUG: 1
      PHP_EXTENSION_XDEBUG: 1
      PHP_EXTENSION_PGSQL: 1
      PHP_EXTENSION_PDO_PGSQL: 1
      PHP_EXTENSION_MYSQLI: 0
      PHP_EXTENSION_GD: 1
      PHP_EXTENSION_INTL: 1
      STARTUP_COMMAND_1: composer install --ignore-platform-reqs
      #STARTUP_COMMAND_2: NETTE_DEBUG=1 php bin/console migrations:migrate --no-interaction --allow-no-migration
      #STARTUP_COMMAND_3: NETTE_DEBUG=1 php bin/console doctrine:fixtures:load --no-interaction
```

## Creating Docker repository
```
# Vypíše existující images
docker images

# Vytvoření tagu
docker tag thecodingmachine/php:8.1-v4-apache  registry.gitlab.com/leheckaj/docker-registry-test/apachegitlab:test 


# Přihlášení k docker registry na Gitlabu, nutné mít Access Token na: read_registry, write_registry
docker login registry.gitlab.com
# Alternativa
docker login registry.gitlab.com -u leheckaj -p TOKEN

# Pushnutí image
docker push registry.gitlab.com/leheckaj/docker-registry-test/apachegitlab:test 

################
## Dobrovolné ##
################

# Zjistím ID naší image: registry.gitlab.com/leheckaj/docker-registry-test/apachegitlab:test ==>  acbddcaf181b
docker images

# Smažeme image
docker rmi -f acbddcaf181b

# Ověříme, že jsme image odstranili
docker images

# Pullneme image z docker registry
docker pull registry.gitlab.com/leheckaj/docker-registry-test/apachegitlab:test

# Vytvoření docker-compose.yml
```

## Creating docker-compose.yml

Docker compose file will be based on previous image. Changing IMAGE is only needed.
Paste this docker-compose.yml file next to the app directory (on the same level).
```
version: '3.8'
services:
  php-apache-environment:
    container_name: DOCKERTEST_apache
    image: registry.gitlab.com/leheckaj/docker-registry-test/apachegitlab:test
    volumes:
      - ./:/var/www/html
    ports:
      - 80:80
    links:
      - database
    depends_on:
      - database
    environment:
      APACHE_DOCUMENT_ROOT: /var/www/html
      NETTE_DEBUG: 1
      PHP_EXTENSION_XDEBUG: 1
      PHP_EXTENSION_PGSQL: 1
      PHP_EXTENSION_PDO_PGSQL: 1
      PHP_EXTENSION_MYSQLI: 0
      PHP_EXTENSION_GD: 1
      PHP_EXTENSION_INTL: 1
      STARTUP_COMMAND_1: composer install --ignore-platform-reqs
      #STARTUP_COMMAND_2: NETTE_DEBUG=1 php bin/console migrations:migrate --no-interaction --allow-no-migration
      #STARTUP_COMMAND_3: NETTE_DEBUG=1 php bin/console doctrine:fixtures:load --no-interaction
```
Nette App will be accessible via: localhost/www
